App.service('AuthorService', function ($http, API) {

	this.getAll = function () {
		return $http.get(API + 'authors');
	}

	this.getAuthorBooks = function (id) {
		return $http.get(API + 'authors/' + id + '/books');
	}

	this.getOne = function (id) {
		return $http.get(API + 'authors/' + id);
	}

	this.save = function (data) {
		return $http.post(API + 'authors', data);
	}

	this.update = function (data) {
		return $http.put(API + 'authors/' + data.id, data);
	}

	this.remove = function (id) {
		return $http.delete(API + 'authors/' + id);
	}

})