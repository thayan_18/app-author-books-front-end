App.service('BookService', function ($http, API) {

	this.getAll = function () {
		return $http.get(API + 'books');
	}

	this.getOne = function (id) {
		return $http.get(API + 'books/' + id);
	}

	this.save = function (data) {
		return $http.post(API + 'books', data);
	}


	this.update = function (data) {
		return $http.put(API + 'books/' + data.id, data);
	}

	this.remove = function (id) {
		return $http.delete(API + 'books/' + id);
	}

})