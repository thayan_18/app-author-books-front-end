App.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'views/authors.html',
            controller: 'AuthorListCtrl'
        })
        .when('/author', {
            templateUrl: 'views/author.html',
            controller: 'AuthorCreateEditCtrl'
        })
        .when('/authors', {
            templateUrl: 'views/authors.html',
            controller: 'AuthorListCtrl'
        })
        .when('/authors/:id', {
            templateUrl: 'views/author.html',
            controller: 'AuthorCreateEditCtrl'
        })
        .when('/authors/:id/books', {
            templateUrl: 'views/books.html',
            controller: 'BookListCtrl'
        })
        .when('/authors/:id/book', {
            templateUrl: 'views/book.html',
            controller: 'BookCreateEditCtrl'
        })
        .when('/authors/:id/book/:idBook', {
            templateUrl: 'views/book.html',
            controller: 'BookCreateEditCtrl'
        })

});