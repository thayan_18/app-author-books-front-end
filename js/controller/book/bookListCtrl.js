App.controller('BookListCtrl', function ($scope, $route, $location, $timeout, $routeParams, AuthorService, BookService) {

    $scope.author = {};
    $scope.books = [];
    $scope.message;

    // init
    (function () {

        AuthorService.getOne($routeParams.id).then(function (result) {
            $scope.author = result.data;
        })

        AuthorService.getAuthorBooks($routeParams.id).then(function (result) {
            $scope.books = result.data;
        })

    })();

    $scope.add = function () {
        $location.path("/authors/" + $routeParams.id + "/book");
    }

    $scope.back = function (id) {
        $location.path("/authors");
    }

    $scope.edit = function (idBook) {
        $location.path("/authors/" + $routeParams.id + "/book/" + idBook);
    }

    $scope.remove = function (id) {
        BookService.remove(id).then(function (result) {
            if (result.status === 200) {
                $scope.message = "item remove success status " + result.status;
                $timeout(function () {
                    $scope.message = undefined;
                    $route.reload();
                }, 1000);

            }
        });
    }

});



