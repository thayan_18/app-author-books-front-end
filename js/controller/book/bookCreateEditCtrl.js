App.controller('BookCreateEditCtrl', function ($scope, $route, $routeParams, $location, $timeout, BookService, AuthorService) {

    $scope.author = {};
    $scope.book = {};
    $scope.message;

    //init
    (function () {

        AuthorService.getOne($routeParams.id).then(function (result) {
            $scope.author = result.data;
        })

        if ($routeParams.idBook) {
            BookService.getOne($routeParams.idBook).then(function (result) {
                $scope.book = result.data;
            });
        } else {
            $scope.book.authorId = $routeParams.id;
        }

    })();


    $scope.back = function () {
        if ($scope.message) {
            $timeout(function () {
                $location.path("/authors/" + $routeParams.id + "/books");
            }, 1000);
        } else {
            $location.path("/authors/" + $routeParams.id + "/books");
        }

    }

    $scope.save = function () {
        $scope.book.author = {};
        $scope.book.author.id = $scope.author.id;
        BookService.save($scope.book).then(function (result) {
            if (result.status === 200) {
                $scope.message = "success status " + result.status;
                $scope.back();
            }
        });
    }




});
