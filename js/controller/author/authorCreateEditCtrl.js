App.controller('AuthorCreateEditCtrl', function ($scope, $route, $routeParams, $location, $timeout, AuthorService) {

    $scope.author = {};
    $scope.message;

    //init
    (function () {
        if ($routeParams.id) {
            AuthorService.getOne($routeParams.id).then(function (result) {
                $scope.author = result.data;
            });
        }
    })();

    $scope.save = function () {
        AuthorService.save($scope.author).then(function (result) {
            if (result.status === 200) {
                $scope.message = "success status " + result.status;
                $scope.back();
            }
        })
    }

    $scope.back = function () {
        if ($scope.message) {
            $timeout(function () {
                $location.path("/authors");
            }, 1000);
        } else {
            $location.path("/authors");
        }

    }




});
