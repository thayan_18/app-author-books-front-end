App.controller('AuthorListCtrl', function ($scope, $route, $location, $routeParams, $filter, $uibModal, $timeout, AuthorService) {

    $scope.authors = [];

    //pagination
    authorsAux = [];
    $scope.pageIndex = 1;
    $scope.totalPages = 0;
    $scope.descriptionPager = "";

    //sort
    $scope.sortType = '';
    $scope.sortReverse = false;
    $scope.search = '';

    $scope.message;



    // init
    (function () {
        AuthorService.getAll().then(function (result) {
            authorsAux = result.data;
            $scope.setPager(1);
        });
    })();

    $scope.add = function () {
        $location.path("/author");
    }

    $scope.goEdit = function (id) {
        $location.path("/authors/" + id);
    }

    $scope.goBooks = function (id) {
        $location.path("/authors/" + id + "/books");
    }

    $scope.remove = function (id) {

        var modalInstance = $uibModal.open({
            templateUrl: 'views/dialogConfirm.html',
            controller: 'DialogCtrl',
            size: 'sm',
            resolve: { idAuthor: id }
        });

        modalInstance.result.then(function (result) {
            if (result == 'ok') {
                $scope.message = "item removed successfully."
                $timeout(function () {
                    $scope.message = undefined;
                    $route.reload();
                }, 1000);
            } else {
                $scope.message = undefined;
            }
        });
    }

    $scope.getSort = function (propertyName) {
        $scope.sortType = propertyName;
        $scope.sortReverse = !$scope.sortReverse;
        authorsAux = $filter('orderBy')(authorsAux, propertyName, $scope.sortReverse);
        $scope.setPager(1);
    }

    $scope.changeSearch = function () {
        var data = authorsAux.slice();
        var newData = $filter('filter')(data, $scope.search);
        $scope.setPager(1, newData);
    }


    $scope.setPager = function (pageIndex, newData) {
        var pageSize = 5;
        var dataAuthorsAux = authorsAux;

        if (newData) {
            dataAuthorsAux = newData;
        }

        $scope.totalPages = Math.ceil(dataAuthorsAux.length / pageSize);
        if (pageIndex < 1 || pageIndex > $scope.totalPages) {
            return;
        }
        var data = dataAuthorsAux.slice();
        var startIndex = (pageIndex - 1) * pageSize;
        var totalIems = pageIndex * pageSize;

        if ($scope.totalPages === pageIndex) {
            totalIems = dataAuthorsAux.length;
        }

        $scope.authors = data.splice(startIndex, pageSize);
        $scope.descriptionPager = "Showing " + (startIndex + 1) + " to " + totalIems + " of " + dataAuthorsAux.length + " entries";
    }


});

App.controller('DialogCtrl', function ($scope, $uibModalInstance, AuthorService, idAuthor) {

    $scope.remove = function () {
        AuthorService.remove(idAuthor).then(function (result) {
            if (result.status === 200) {
                $uibModalInstance.close('ok');
            }
        });

    };

    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };
});






